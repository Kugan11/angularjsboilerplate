(function(){
    var app = angular.module("app");
    
    app.directive("navBar", [
        function(){
            return {
                restrict: 'E',
                template: `
                <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                  <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                      <img alt="XYZ" ng-src="/images/favicon-32x32.png" height="25" width="25">
                    </a>
                      <ul class="nav navbar-nav">
                          <li>
                            <a ui-sref="home" ui-sref-active="active" href="/home" class="text-uppercase">home</a>
                          </li>
          
                          <li>
                            <a ui-sref="aboutUs" ui-sref-active="active" href="/aboutus" class="text-uppercase">about us</a>
                          </li>
                          
                          <li>
                            <a ui-sref="showcaseComponents" ui-sref-active="active" href="/showcasecomponents" class="text-uppercase">showcase components</a>
                          </li>
                      </ul>
                  </div>
                </div>
              </nav>
                `
            };
        }
    ]);

})();