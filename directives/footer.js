(function(){
    var app = angular.module("app");
    
    app.directive("footer", [
        function(){
            let year = new Date().getUTCFullYear();
            
            return {
                restrict: 'E',
                template: 
                '<nav class="navbar navbar-inverse navbar-fixed-bottom">' +
                    '<div class="navbar-text small"> COPYRIGHT XYZ ' + year + '</div>' +
                '</nav>'
            };
        }
    ]);

})();