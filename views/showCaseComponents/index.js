(function(){

    var controllerId = "views.showCaseComponents.index";

    var app = angular.module('app');
    app.controller(controllerId, [
        '$window', '$uibModal',
        function($window, $uibModal) {
            function init(){
                $window.document.title = "Show case components";
            };

            var vm = this;
            vm.contents = [];
            vm.loading = 0;
 
            vm.openModal = function() {

                //open modal and keep a reference of the instance
                let modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/showCaseComponents/showUsersModal.html',
                    controller: 'views.showCaseComponents.showUsersModal',
                    controllerAs: 'vm',
                    size: 'lg',
                    resolve: {
                        dataFromCaller: function () {
                            return 2; //e.g: userId: 2
                        }
                    }
                }); 
                
                //callbacks after modal closed
                modalInstance.result
                .then(function (data) {
                        vm.contents.push(Date() + " - " + data);
                    }, function () {
                        console.log("modal dimissed at " + Date());
                    }
                );

            };

            init();
        }
    ]);
    
})();