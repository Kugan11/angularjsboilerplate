(function(){
  var controllerId = "views.showCaseComponents.showUsersModal";
  var app = angular.module('app');

  app.controller(controllerId, [
    '$uibModalInstance', 'dataFromCaller', '$http',
    function ($uibModalInstance, dataFromCaller, $http) {
      function init(){
        vm.userId = dataFromCaller;
        vm.getUser(vm.userId);
      };

      var vm = this;
      vm.loading = 0;
      vm.content = "";
      vm.user = null;

      vm.getUser = function(userId) {
        vm.loading++;

        $http
         .get("https://reqres.in/api/users/" + userId)
         .then(
            function successCallback(response) {
                vm.user = response? response.data.data : null;
            },
            function errorCallback(response) {
                console.log("Unable to perform get request");
            }
         )
         .finally(function(){
             vm.loading--;
         }); 
      };

      vm.ok = function () {
        let dataReturnToCaller = "I have been passed from modal!"; 
        $uibModalInstance.close(dataReturnToCaller);
      };
      
      vm.cancel = function () {
        $uibModalInstance.dismiss();
      };

      init();
    }
  ]);

})();