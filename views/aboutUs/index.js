(function(){
    var controllerId = "views.aboutUs.index";

    var app = angular.module("app");
    app.controller(controllerId, [
        '$window',
        function($window){
            function init(){
                $window.document.title = "About us";
            };
            
            var vm = this;

            vm.content = `
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non rhoncus nisi. Praesent commodo tincidunt suscipit. Nam iaculis diam lectus, vel eleifend augue interdum et. Ut vulputate nibh gravida efficitur porta. Praesent laoreet, diam vitae feugiat dictum, turpis erat pellentesque odio, quis rutrum enim dolor at sem. Donec et vestibulum quam. Proin sit amet faucibus magna, sed placerat lorem. Praesent sit amet enim enim. Suspendisse tempor aliquet dui id rutrum.

                Etiam malesuada metus et massa congue facilisis quis et ex. Maecenas pharetra massa sit amet turpis fermentum venenatis. Etiam vel venenatis ex. Pellentesque eleifend id augue a luctus. Morbi condimentum ullamcorper ante, ac porta dolor facilisis nec. Quisque massa orci, tempor vulputate semper a, euismod vitae ligula. Phasellus dictum eu sapien at vehicula. Pellentesque hendrerit vel sem consectetur mollis. Morbi pharetra eu leo sit amet auctor. Donec cursus tortor dui, in ultrices arcu laoreet sed. Nunc a sagittis mi. Suspendisse convallis massa tortor, et lacinia lorem vestibulum vel.
                
                Sed euismod condimentum laoreet. Sed interdum dui sapien, non maximus risus dictum at. Vestibulum bibendum pulvinar libero auctor sodales. Morbi at suscipit turpis. Suspendisse ac congue nunc. Mauris dictum dui lacus, sit amet molestie nunc volutpat non. Cras et accumsan ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec vehicula elementum aliquam. Nulla et blandit sapien, et euismod nulla. Nam sit amet aliquet sem. Nullam ultricies venenatis urna at lacinia.
                
                Nam at sapien vitae nisl lacinia rhoncus at id lacus. Etiam accumsan ipsum vitae eros blandit, eu fringilla nunc pretium. Quisque fringilla mauris non orci luctus efficitur eget id libero. Phasellus eu aliquet lorem. Sed in rutrum sem. Vivamus bibendum finibus hendrerit. Praesent aliquet sapien vulputate consequat sagittis. Fusce ac metus cursus, rutrum nisi eu, euismod mauris. Donec lobortis dui in sodales vehicula. Phasellus pretium lacus neque, vel tincidunt turpis luctus sit amet. Ut vel fringilla nunc. Nunc viverra dui vel fringilla placerat. Aliquam tincidunt gravida sem, sed malesuada est elementum ut. Maecenas vel arcu tempor, commodo nisl sit amet, consectetur urna. Suspendisse ultricies porttitor vulputate.
                
                In varius ante id aliquam pretium. Nunc pellentesque odio non purus tempus efficitur. Cras ac metus eget mauris varius luctus. Morbi ut ipsum vel justo feugiat iaculis ac non tellus. Vestibulum cursus fringilla libero, vitae pretium lorem aliquam in. Praesent neque felis, laoreet in eleifend non, elementum ut eros. Quisque sapien sem, imperdiet ut diam id, placerat pretium enim. Proin vitae erat elementum, suscipit leo sed, ultrices leo. Mauris suscipit ex quis commodo auctor. Morbi tristique pharetra varius. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
            `;

            init();
        }
    ]);
})();