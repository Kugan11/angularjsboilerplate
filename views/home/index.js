(function(){

    var controllerId = "views.home.index";

    var app = angular.module('app');
    app.controller(controllerId, [
        '$http', '$window',
        function($http, $window) {
            function init(){
                $window.document.title = "Home";
            };

            var vm = this;
            vm.loading = 0;
 
            vm.getUsers = function(page) {
                vm.loading++;

                vm.page = page;
                
                $http
                 .get("https://reqres.in/api/users" + "?page=" + page)
                 .then(
                    function successCallback(response) {
                        vm.users = response? response.data.data : null;
                    },
                    function errorCallback(response) {
                        console.log("Unable to perform get request");
                    }
                 )
                 .finally(function(){
                     vm.loading--;
                 }); 
            };

            init();
        }
    ]);
    
})();