(function(){
    'use strict';

    var app = angular.module('app', [
        'ui.router',
        'ui.bootstrap'
    ]);
    
    app.config([
        '$urlRouterProvider','$stateProvider', '$locationProvider',
        function($urlRouterProvider, $stateProvider, $locationProvider) {

            $locationProvider.hashPrefix('');
            $urlRouterProvider.otherwise('/');
            $locationProvider.html5Mode(true);

            $stateProvider
                .state('index', {
                    url: '/',
                    templateUrl: '/views/index.html',
                    redirectTo: "home"   
                })
                .state('home', {
                    url: '/home',
                    templateUrl: '/views/home/index.html' 
                })
                .state('aboutUs', {
                    url: '/aboutus',
                    templateUrl: '/views/aboutUs/index.html' 
                })
                .state('showcaseComponents', {
                    url: '/showcasecomponents',
                    templateUrl: '/views/showcaseComponents/index.html' 
                });
        }
    ]);
    
    app.run([
        '$window',
        function($window) {
            $window.document.title = "XYZ";
        }
    ]);

})();